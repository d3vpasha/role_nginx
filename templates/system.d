[Unit]
Description=A high performance web server and a reverse proxy server
Documentation=man:nginx(8)
After=network.target

[Service]
Type=forking
PIDFile={{ nginx_pid_path }}
ExecStartPre={{ nginx_exec_path }} -t -q -g 'daemon on; master_process on;'
ExecStart={{ nginx_exec_path }} -g 'daemon on; master_process on;'
ExecReload={{ nginx_exec_path }} -g 'daemon on; master_process on;' -s reload
ExecStop=-/sbin/start-stop-daemon --quiet --stop --retry QUIT/5 --pidfile {{ nginx_pid_path }}
TimeoutStopSec=5
KillMode=mixed

[Install]
WantedBy=multi-user.target
