Role Name
=========

Installs Nginx on Ubuntu.

This role downloads sources from Nginx official website. It also installs and configures v1.12.1.

Requirements
------------

You must have Python installed on target server if you want to install Nginx through Ansible.

Role Variables
--------------

NA

Dependencies
------------

NA

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: all
      become: yes
      become_user: root
      roles:
         - { role: role_nginx }

License
-------

BSD

Author Information
------------------

This role was created in 2017 by [Mehmet DEMIR](https://d3vpasha.wordpress.com/).
